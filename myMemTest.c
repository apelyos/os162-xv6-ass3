/*
 * test.c
 *
 *  Created on: May 16, 2016
 *      Author: yanivha6
 */


#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"

#define PGSIZE 4096
#define PAGE_TO_ALLOC 15

#define USER_MODE 0
#define SERIAL_MODE 1
#define FORK_MODE 2

#define USER_BLOCKS 5

int stdout = 1;

unsigned long randstate = 1;
unsigned int
rand()
{
  randstate = randstate * 1664525 + 1013904223;
  return randstate;
}

//this program receives profiles of checking
//profiles:
//0. Normal user -- will access some areas more then others -- will access the 5 first block and a random access
//1. Serial user -- will access all areas Serially
//2. Fork & Normal mode
//3. Fork & Serial mode

int main(int argc, char *argv[])
{
	int i;
	int * alloced[PAGE_TO_ALLOC];
	int profile;
	int pid;

	if(argc != 2){
		profile = USER_MODE;
	} else {
		profile = atoi(argv[1]);
	}

	printf(stdout, "profile is %d\n" , profile);

	for (i = 0 ; i < PAGE_TO_ALLOC ; ++i){
		printf(stdout, "creating block number %d\n", i);
		alloced[i] = malloc(PGSIZE);
		*(alloced[i]) = i;  //make the first bytes in this allocated block to contain the block number
		printf(stdout, "accessing block %d\n", *(alloced[i]));
	}

	printf(stdout, "\n\n~~starting test~~\n\n");

	if(FORK_MODE & profile){
	  printf(stdout, "forking...\n");
		pid = fork();
		if(pid == 0){
			sleep(1500);
			printf(stdout, "\n\n~~son begin~~\n\n");
		} else {
	      	for (i = PAGE_TO_ALLOC-1; i >= 0 ; --i)
				*(alloced[i]) *= 100;
			printf(stdout, "\n\n~~father begin~~\n\n");
		}
	}
	if(SERIAL_MODE & profile){
		for (i = PAGE_TO_ALLOC-1; i >= 0 ; --i){
			printf(stdout, "accessing block %d\n", *(alloced[i]));
		}
		for (i = 0 ; i < PAGE_TO_ALLOC ; ++i){
			printf(stdout, "accessing block %d\n", *(alloced[i]));
		}
	} else {
		for(i = 0 ; i < 50 ; ++i){
			printf(stdout, "accessing block %d\n", *(alloced[i % USER_BLOCKS]));
			printf(stdout, "accessing block %d\n", *(alloced[rand() % PAGE_TO_ALLOC]));
		}
	}

	printf(stdout, "\n~~deleting block\n");
	for (i = 0 ; i < PAGE_TO_ALLOC ; ++i){
		free(alloced[i]);
	}

	if((FORK_MODE & profile) && pid != 0)
		wait();

	exit();
}

