#include "param.h"
#include "types.h"
#include "defs.h"
#include "x86.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "elf.h"

extern char data[];  // defined by kernel.ld
pde_t *kpgdir;  // for use in scheduler()
struct segdesc gdt[NSEGS];

// Set up CPU's kernel segment descriptors.
// Run once on entry on each CPU.
void
seginit(void)
{
  struct cpu *c;

  // Map "logical" addresses to virtual addresses using identity map.
  // Cannot share a CODE descriptor for both kernel and user
  // because it would have to have DPL_USR, but the CPU forbids
  // an interrupt from CPL=0 to DPL=3.
  c = &cpus[cpunum()];
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);

  // Map cpu, and curproc
  c->gdt[SEG_KCPU] = SEG(STA_W, &c->cpu, 8, 0);

  lgdt(c->gdt, sizeof(c->gdt));
  loadgs(SEG_KCPU << 3);

  // Initialize cpu-local storage.
  cpu = c;
  proc = 0;
}

// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
  if(*pde & PTE_P){
    pgtab = (pte_t*)p2v(PTE_ADDR(*pde));
  } else {
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
      return 0;
    // Make sure all those PTE_P bits are zero.
    memset(pgtab, 0, PGSIZE);
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table
    // entries, if necessary.
    *pde = v2p(pgtab) | PTE_P | PTE_W | PTE_U;
  }
  return &pgtab[PTX(va)];
}

// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
  char *a, *last;
  pte_t *pte;

  a = (char*)PGROUNDDOWN((uint)va);
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
  for(;;){
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
    if(a == last)
      break;
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
}

// There is one page table per process, plus one that's used when
// a CPU is not running any process (kpgdir). The kernel uses the
// current process's page table during system calls and interrupts;
// page protection bits prevent user code from using the kernel's
// mappings.
//
// setupkvm() and exec() set up every page table like this:
//
//   0..KERNBASE: user memory (text+data+stack+heap), mapped to
//                phys memory allocated by the kernel
//   KERNBASE..KERNBASE+EXTMEM: mapped to 0..EXTMEM (for I/O space)
//   KERNBASE+EXTMEM..data: mapped to EXTMEM..V2P(data)
//                for the kernel's instructions and r/o data
//   data..KERNBASE+PHYSTOP: mapped to V2P(data)..PHYSTOP,
//                                  rw data + free physical memory
//   0xfe000000..0: mapped direct (devices such as ioapic)
//
// The kernel allocates physical memory for its heap and for user memory
// between V2P(end) and the end of physical memory (PHYSTOP)
// (directly addressable from end..P2V(PHYSTOP)).

// This table defines the kernel's mappings, which are present in
// every process's page table.
static struct kmap {
  void *virt;
  uint phys_start;
  uint phys_end;
  int perm;
} kmap[] = {
    { (void*)KERNBASE, 0,             EXTMEM,    PTE_W}, // I/O space
    { (void*)KERNLINK, V2P(KERNLINK), V2P(data), 0},     // kern text+rodata
    { (void*)data,     V2P(data),     PHYSTOP,   PTE_W}, // kern data+memory
    { (void*)DEVSPACE, DEVSPACE,      0,         PTE_W}, // more devices
};

// Set up kernel part of a page table.
pde_t*
setupkvm(void)
{
  pde_t *pgdir;
  struct kmap *k;

  if((pgdir = (pde_t*)kalloc()) == 0)
    return 0;
  memset(pgdir, 0, PGSIZE);
  if (p2v(PHYSTOP) > (void*)DEVSPACE)
    panic("PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
        (uint)k->phys_start, k->perm) < 0)
      return 0;
  return pgdir;
}

// Allocate one page table for the machine for the kernel address
// space for scheduler processes.
void
kvmalloc(void)
{
  kpgdir = setupkvm();
  switchkvm();
}

// Switch h/w page table register to the kernel-only page table,
// for when no process is running.
void
switchkvm(void)
{
  lcr3(v2p(kpgdir));   // switch to the kernel page table
}

// Switch TSS and h/w page table to correspond to process p.
void
switchuvm(struct proc *p)
{
  pushcli();
  cpu->gdt[SEG_TSS] = SEG16(STS_T32A, &cpu->ts, sizeof(cpu->ts)-1, 0);
  cpu->gdt[SEG_TSS].s = 0;
  cpu->ts.ss0 = SEG_KDATA << 3;
  cpu->ts.esp0 = (uint)proc->kstack + KSTACKSIZE;
  ltr(SEG_TSS << 3);
  if(p->pgdir == 0)
    panic("switchuvm: no pgdir");
  lcr3(v2p(p->pgdir));  // switch to new address space
  popcli();
}

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void
inituvm(pde_t *pgdir, char *init, uint sz)
{
  char *mem;

  if(sz >= PGSIZE)
    panic("inituvm: more than a page");
  mem = kalloc();
  memset(mem, 0, PGSIZE);
  mappages(pgdir, 0, PGSIZE, v2p(mem), PTE_W|PTE_U);
  memmove(mem, init, sz);
}

// Load a program segment into pgdir.  addr must be page-aligned
// and the pages from addr to addr+sz must already be mapped.
int
loaduvm(pde_t *pgdir, char *addr, struct inode *ip, uint offset, uint sz)
{
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
    panic("loaduvm: addr must be page aligned");
  for(i = 0; i < sz; i += PGSIZE){
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
      panic("loaduvm: address should exist");
    pa = PTE_ADDR(*pte);
    if(sz - i < PGSIZE)
      n = sz - i;
    else
      n = PGSIZE;
    if(readi(ip, p2v(pa), offset+i, n) != n)
      return -1;
  }
  return 0;
}

// Allocate page tables and physical memory to grow process from oldsz to
// newsz, which need not be page aligned.  Returns new size or 0 on error.
int
allocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
  char *mem;
  uint a;

  if(newsz >= KERNBASE)
    return 0;
  if(newsz < oldsz)
    return oldsz;

  a = PGROUNDUP(oldsz);
  for(; a < newsz; a += PGSIZE){
    //new- check if the process as reach the limits of pages in the RAM
    if(proc->pgalloc == MAX_PSYC_PAGES){
      swapout(selectpg());
    }

    mem = kalloc();
    if(mem == 0){
      cprintf("allocuvm out of memory\n");
      deallocuvm(pgdir, newsz, oldsz, proc);
      return 0;
    }
    memset(mem, 0, PGSIZE);
    mappages(pgdir, (char*)a, PGSIZE, v2p(mem), PTE_W|PTE_U);
    //new- update numbers
    ++proc->pgalloc;
    add_pg_entry(a);

    if(proc->pgalloc + proc->pgout > MAX_TOTAL_PAGES)
      panic("program has allocated too much pages");

  }
  return newsz;
}

// Deallocate user pages to bring the process size from oldsz to
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz, struct proc *p)
{
  pte_t *pte;
  uint a, pa;
  //cprintf("in dealloc, %d %d\n", oldsz, newsz);

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
  for(; a  < oldsz; a += PGSIZE){
    pte = walkpgdir(pgdir, (char*)a, 0);
    if(!pte)
      a += (NPTENTRIES - 1) * PGSIZE;
    else if((*pte & PTE_P) != 0){
      pa = PTE_ADDR(*pte);
      if(pa == 0)
        panic("kfree");
      char *v = p2v(pa);
      kfree(v);
      *pte = 0;
      //new- update counter
      if (p) {
        --p->pgalloc;
        remove_pg_entry(a, p);
      }
    }
  }
  return newsz;
}

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir, struct proc *p)
{
  uint i;
  //cprintf("in freevm, pid: %d\n", p->pid);

  if(pgdir == 0)
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0, p);
  for(i = 0; i < NPDENTRIES; i++){
    if(pgdir[i] & PTE_P){
      char * v = p2v(PTE_ADDR(pgdir[i]));
      kfree(v);
    }
  }
  kfree((char*)pgdir);
}

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
  if(pte == 0)
    panic("clearpteu");
  *pte &= ~PTE_U;
}

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz, struct proc *child)
{
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;
  pte_t *new_pte;

  if((d = setupkvm()) == 0)
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
      panic("copyuvm: pte should exist");

    // handle swapped pages
    if(!(*pte & PTE_P)){
    	if((*pte & PTE_PG)){
    	  if((new_pte = walkpgdir(d, (void *) i, 0)) == 0)
    	    panic("copyuvm: new_pte");

    		*new_pte = *pte;
    		continue;
    	}
    	panic("copyuvm: page not present");
    }

    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
    memmove(mem, (char*)p2v(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, v2p(mem), flags) < 0)
      goto bad;
  }
  return d;

  bad:
  freevm(d, child);
  return 0;
}

//PAGEBREAK!
// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
  if((*pte & PTE_P) == 0)
    return 0;
  if((*pte & PTE_U) == 0)
    return 0;
  return (char*)p2v(PTE_ADDR(*pte));
}

// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
    va0 = (uint)PGROUNDDOWN(va);
    pa0 = uva2ka(pgdir, (char*)va0);
    if(pa0 == 0)
      return -1;
    n = PGSIZE - (va - va0);
    if(n > len)
      n = len;
    memmove(pa0 + (va - va0), buf, n);
    len -= n;
    buf += n;
    va = va0 + PGSIZE;
  }
  return 0;
}

// returns next page to be swapped
// this will have different implementation
// according to the selection policy
// -> then when need to page out, call swapout(selectpg());
#ifdef SELECT_NONE
uint selectpg() {
  return -1;
}
#endif


#ifdef SELECT_FIFO
uint selectpg() {
  int i,  min_idx = -1;
  uint min = 0xFFFFFFFF;

  for(i = 0; i < MAX_TOTAL_PAGES ; ++i) {
    if(proc->paging_data[i].used != 0 && proc->paging_data[i].on_disk == 0 && proc->paging_data[i].time_of_creation < min){
      min = proc->paging_data[i].time_of_creation;
      min_idx = i;
    }
  }
  if(min_idx == -1)
    panic("fifo: couldn't select page");

  return proc->paging_data[min_idx].va;
}
#endif

#ifdef SELECT_SCFIFO
uint selectpg() {
  int i,  min_idx = -1;
  uint min;
  pte_t *pte;
  while(1){
	  min = 0xFFFFFFFF;
	  for(i = 0; i < MAX_TOTAL_PAGES ; ++i) {
		if(proc->paging_data[i].used != 0 && proc->paging_data[i].on_disk == 0 && proc->paging_data[i].time_of_creation < min){
		  min = proc->paging_data[i].time_of_creation;
		  min_idx = i;
		}
	  }
	  if(min_idx == -1)
		panic("scfifo: couldn't select page");

	  pte = walkpgdir(proc->pgdir, (void*)proc->paging_data[i].va, 0);
	  if (*pte & PTE_A) { // accessed
		*pte = *pte & ~PTE_A;
		proc->paging_data[min_idx].time_of_creation = ticks;
	  } else {
		break;
	  }
  }

  return proc->paging_data[min_idx].va;
}
#endif


#ifdef SELECT_NFU
uint selectpg() {
  int i,  min_idx = -1;
  uint min = 0xFFFFFFFF;

  for(i = 0; i < MAX_TOTAL_PAGES ; ++i) {
    if(proc->paging_data[i].used != 0 && proc->paging_data[i].on_disk == 0 && proc->paging_data[i].last_used < min){
      min = proc->paging_data[i].last_used;
      min_idx = i;
    }
  }
  if(min_idx == -1)
    panic("nfu: couldn't select page");

  return proc->paging_data[min_idx].va;
}
#endif



// swaps to disk the given page
// if va = 0 : do nothing
int find_next_place_on_file(struct proc *p){
  int i,j, found;
  for (i = 0 ; i < MAX_PSYC_PAGES + 1; ++i){
    found = 0;
    for(j = 0 ; j < MAX_TOTAL_PAGES; ++j)
      if(p->paging_data[j].used != 0 && p->paging_data[j].on_disk != 0 && p->paging_data[j].place_on_disk == i){
        found = 1;
      }
    if(found == 0)
      return i;
  }
  int on_disk = 0 , on_ram = 0;
  for(i = 0; i < MAX_TOTAL_PAGES ; ++i) {
    cprintf("for iteration %d\n", i);
    cprintf("proc->paging_data[i].used = %d\n", p->paging_data[i].used);
    cprintf("proc->paging_data[i].on_disk = %d\n", p->paging_data[i].on_disk);
    cprintf("proc->paging_data[i].time_of_creation = %d\n", p->paging_data[i].time_of_creation);
    cprintf("proc->paging_data[i].place_on_disk = %d\n", p->paging_data[i].place_on_disk);
    if(p->paging_data[i].used != 0){
      if(p->paging_data[i].on_disk != 0){
        on_disk++;
      } else {
        on_ram++;
      }
    }
  }

  cprintf("p->pgalloc = %d\n", p->pgalloc);
  cprintf("p->pgout = %d\n", p->pgout);
  cprintf("on_disk = %d\n" , on_disk);
  cprintf("on_ram = %d\n" , on_ram);
  panic("no place to put on disk");
  return -1;
}

int swapout(uint va) {
  int i;
  pte_t *pte;
  int file_index;

  if (va == -1)
    return 0;

  for(i = 0; i < MAX_TOTAL_PAGES ; ++i) {
    if(proc->paging_data[i].va == va && proc->paging_data[i].used != 0) {
      file_index = find_next_place_on_file(proc);
      if(writeToSwapFile(proc, (char*)va, PGSIZE * file_index, PGSIZE) == -1)
        panic("couldn't write to file");
      //writeToSwapFile(proc, uva2ka(proc->pgdir, (char*)va), PGSIZE * i, PGSIZE);
      proc->paging_data[i].on_disk = 1;
      proc->paging_data[i].place_on_disk = file_index;

      if((pte = walkpgdir(proc->pgdir, (void*)va, 0)) == 0)
        panic("can't access virtual addr pte, in swapout");
      if(!(*pte & PTE_P)) {
        panic("not present ,in swapout");
      }
      // save flags
      proc->paging_data[i].flags = PTE_FLAGS(*pte);

      kfree(p2v(PTE_ADDR(*pte)));
      // mark pte as "not present" and "swapped"
      *pte = (*pte & (~PTE_P)) | PTE_PG ;

      --proc->pgalloc;
      ++proc->pgout;
      ++proc->pgouttotal;

      // refresh TLB
      lcr3(v2p(proc->pgdir));

      return 1;
    }
  }

  return 0;
}

// swaps from disk to memory the given page
int swapin(uint faultaddr) {
  faultaddr = PTE_ADDR(faultaddr);

  //allocate new physical address
  char *new_page = kalloc();
  uint index_slot = get_index_slot(faultaddr);
  //read page to new allocated physical address
  readFromSwapFile(proc, new_page, proc->paging_data[index_slot].place_on_disk * PGSIZE, PGSIZE);
  //set new address in page table
  mappages(proc->pgdir, (void*)faultaddr, PGSIZE, (uint)v2p(new_page), proc->paging_data[index_slot].flags);
  ++proc->pgalloc;
  --proc->pgout;
  proc->paging_data[index_slot].on_disk = 0;
  proc->paging_data[index_slot].time_of_creation = ticks;
  proc->paging_data[index_slot].last_used = ticks;

  return 0;
}

// page fault trap goes here
// return 0 on error
int pagetrap(uint faultaddr) {
  pte_t *pte;

  pte = walkpgdir(proc->pgdir, (void*) faultaddr, 0);
  if (*pte & PTE_PG) { // paged out to storage
    ++proc->pgfaults;
    if(proc->pgalloc == MAX_PSYC_PAGES)
      swapout(selectpg());
    swapin(faultaddr);
  } else {
    // copied from trap.c
    // process misbehaved
    cprintf("pid %d %s: trap %d err %d on cpu %d "
            "eip 0x%x addr 0x%x pte %d--kill proc\n",
            proc->pid, proc->name, proc->tf->trapno, proc->tf->err, cpu->id, proc->tf->eip,
            faultaddr, pte);
    proc->killed = 1;
  }
  return 0;
}

uint get_index_slot(uint va){
  uint addr = va & 0xFFFFF000;
  int i = 0;
  for(; i < MAX_TOTAL_PAGES ; ++i)
    if(proc->paging_data[i].used != 0 && (uint)proc->paging_data[i].va == addr){
      if (proc->paging_data[i].on_disk == 0)
        panic("get_idx : not on disk");

      return i;
    }
  panic("can't find page: get_file_location");
}

int add_pg_entry(uint a) {
  int i;

  for(i = 0; i < MAX_TOTAL_PAGES ; ++i) {
    if(proc->paging_data[i].used == 0) {
      proc->paging_data[i].on_disk = 0;
      proc->paging_data[i].time_of_creation = ticks;
      proc->paging_data[i].last_used = 0;
      proc->paging_data[i].va = a;
      proc->paging_data[i].used = 1;
      proc->paging_data[i].flags = 0;
      return 1;
    }
  }

  panic("no free pg entry");
  return 0;
}

void reset_pg_table(struct proc *p) {
  int i;

  for(i = 0; i < MAX_TOTAL_PAGES ; ++i) {
    p->paging_data[i].used = 0;
    p->paging_data[i].on_disk = 0;
  }
}

int remove_pg_entry(uint addr, struct proc *p) {
  int i;

  for(i = 0; i < MAX_TOTAL_PAGES ; ++i) {
    if(p->paging_data[i].va == addr && p->paging_data[i].used != 0) {
      p->paging_data[i].va = 0;
      p->paging_data[i].used = 0;
      return 1;
    }
  }

  //cprintf("proc that exit: %d\n", p->pid);
  //panic("error remove pg entry");
  return 0;
}

void update_lru_pages(struct proc* p) {
  #define MSB 0x8000
  int i;
  pte_t *pte;

  for(i = 0; i < MAX_TOTAL_PAGES ; ++i) {
    if(p->paging_data[i].used == 0)
      continue;

    p->paging_data[i].last_used >>= 1;

    pte = walkpgdir(proc->pgdir, (void*)p->paging_data[i].va, 0);
    if (pte == 0)
      continue; //panic?

    if (*pte & PTE_A) { // accessed
      p->paging_data[i].last_used |= MSB;
      *pte = *pte & ~PTE_A;
      //cprintf("updating..%d\n", p->paging_data[i].va);
    }
  }

}
